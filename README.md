# Projecto da UC de Desenvolvimento de Software
## Ideias iniciais:

* API Unity para jogos 2D e 3D
* Título: Extreme G7 Racing
* Tema:   Corrida de carros

Extreme G7 Racing

Pretendemos construir um jogo de corridas em que o utilizador controla um carro numa pista fechada (oval por defeito), usando a API Unity3D. 

Para a escolha da arquitectura deste projecto, tivemos em atenção, entre outros, os seguintes fatores:

* Necessidade de dividir as user stories entre os elementos da equipa, que deverão exercer diversas funções, como o design ou a programação, permitindo a flexibilidade de o trabalho ser desenvolvido em simultâneo.
* Criação de componentes que possam ser reutilizados em projetos futuros.
* Flexibilidade de mudança de interfaces gráficas sem necessidade de refactoring do código base.


Essa adaptação será feita da seguinte forma:

O Model inclui dados que definem os vários artefatos do jogo. Que poderão ser de um carro, de um edifício, de um painel de classificação etc. Utilizemos o carro como exemplo:

Dos seus dados farão parte as caraterísticas do carro, como por exemplo a cor, a sua posição no ecrã, danos recebidos, entre outros.

A View será o componente MVC em que o "Unity" terá mais influência, através da "inversão de controlo" e onde, pelas suas classes, teremos o input e o output.

O input será feito através do teclado ou touch screen, conforme se quiser distribuir o jogo para PC ou para mobile. O output incluirá tudo o que engloba a parte gráfica e sonora do jogo.
A View terá eventos relacionados com o input para a manipulação no carro, onde por exemplo ao pressionar a tecla para avançar do ponto A para o ponto B, será acionada a respectiva função, mas esta não será a responsável por essa deslocação.

O Controller será a ligação entre o Model e a View. Este componente utilizará os dados do Model e notificará a View de como se deverá atualizar. Por outro lado, também utilizará os eventos despoletados na View e notificará o Model a efetuar a atualização.

![ ](Mediating-Controller_MVC.png  "Mediating-Controller_MVC")


### As stories de cada sprint vão ser organizadas em:
http://www.bitbucketcards.com/g7dsuab2016/app

### O backlog criado com todas as user stories organizadas por interesse de negócio estão na spreadsheet:
https://docs.google.com/spreadsheets/d/1N1uBsk1eu7vkCHoQskwX1oji2GB_8QtJ3ZypjU9u5m8/edit#gid=0

### Workflow de código:
https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow